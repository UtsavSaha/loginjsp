package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import validator.UserValidator;


@WebServlet(name = "AnnotationExample",
description = "Example Servlet Using Annotations",
urlPatterns = {"/Home"}
)
public class LoginController extends HttpServlet  {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userName = req.getParameter("un");
		String password = req.getParameter("pw");
		if(userName == null ||  userName.trim().equals("") ||  password == null || password.trim().equals("")) {
			 resp.sendRedirect("invalidLogin.jsp"); 
			 return;
		}//error page 
		try
		{	    

		   Boolean isCredentialsCorrect = new UserValidator().matchCredentials(userName, password);
		    
			   		    
		     if (isCredentialsCorrect)
		     {
			        
		          HttpSession session = req.getSession(true);	    
		          session.setAttribute("currentSessionUser",req.getParameter("un")); 
		          System.out.println("vlid user logged in. routing to home page");
		          resp.sendRedirect("Home.jsp"); 
		          return;//logged-in page      		
		     }
			        
		     else 
		    	 resp.sendRedirect("invalidLogin.jsp"); 
		     return;//error page 
		} 
				
				
		catch (Throwable theException) 	    
		{
		     System.out.println(theException); 
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
