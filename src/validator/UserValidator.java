package validator;

import Dao.UserDao;
import Model.User;

public class UserValidator {

	public boolean matchCredentials(String userName , String password) {
		
		User user = new UserDao().getUserDetailsByUserName(userName);
		if(user.getPassword().matches(password)) {
			return true;
		}
		return false;
	}
}
