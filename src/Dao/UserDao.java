package Dao;

import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Model.User;

public class UserDao {
	

	public User getUserDetailsByUserName(String userName) {
		
		
		String fileName = "Data/Users.json";
		String file = this.getClass().getClassLoader()
                .getResource(fileName).getFile();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(new FileReader(file));
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObject = (JSONObject) obj;
		
		if(!jsonObject.containsKey(userName)) {
			return null;
		}
		String password = (String) jsonObject.get(userName);
		return new User(userName, password);
	}
	
	
}
